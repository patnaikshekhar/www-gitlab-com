---
layout: handbook-page-toc
title: Real-time Editing of Issue Descriptions (REID) Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## The Real-time Editing of Issue Descriptions (REID) Single-Engineer Group

The Real-time Editing of Issue Descriptions (REID) SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

This group is focussed on Real-time collaborative editing of issue descriptions and will build upon the recent work to [view real-time updates of assignee in issue sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/17589).

The goal is to have real-time collaborative editing of issue descriptions in order to turn issues into a Google Docs type of experience.  Initially we will dogfood this at GitLab for meeting agendas and notes.

There are additional ideas within the [Real-time collaboration Epic](https://gitlab.com/groups/gitlab-org/-/epics/2345), however the goal of this SEG is the minimal functionality described above.

## Latest video

<figure class="video_container">
    <iframe width="600" height="340" src="https://www.youtube.com/embed?max-results=1&controls=0&showinfo=0&rel=0&listType=playlist&list=PL05JrBw4t0KpPmRsaVaDOoWyIp1iKacZo" frameborder="0" allowfullscreen></iframe>
</figure>

## Vision

> Make real-time collaboration on GitLab work for everything.

 You should never have to leave GitLab for pair programming, collaborating on tech designs and RFC, creating and editing issues, creating and reviewing merge requests, …

## Roadmap

1. [ ] Foundation
2. [ ] MVP: Isssue Desccriptions
3. [ ] Adoption in all editing interfaces

## Foundation

GitLabs software stack enables real-time collaboration via [Websockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSocket), in particular [GraphQL subscriptions](https://graphql.org/blog/subscriptions-in-graphql-and-relay/). Real-time comes with certain complexities when users want to collaboratively edit text, in particular rich text. In order to allow multiple parties to edit the same *text* concurrently, we must ensure a conflict-free replication mode between all participating clients. There are several ways to achieve this, but we utilize [CRDTs](https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type). CRDTs allow us to concurrently edit state from `1, 2, … n` clients and eventually end up with a consistent representation of the document on all clients.

Every participant in the editing process is a client (in comparison to [Operational Transformation](https://en.wikipedia.org/wiki/Operational_transformation)). The CRDT we picked ([YATA](https://www.researchgate.net/publication/310212186_Near_Real-Time_Peer-to-Peer_Shared_Editing_on_Extensible_Data_Types)) has a popular frontend implementation [Y.js](https://github.com/yjs/yjs) and we have created bindings for its [Rust](https://www.rust-lang.org/) port [`y-crdt`](https://github.com/y-crdt/y-crdt). This allows our Ruby on Rails backend to just act as one more client.

### The `y-rb` gem

**Current State:** Published to [RubyGems](https://rubygems.org/gems/y-rb).

This [Ruby](https://www.ruby-lang.org/en/) gem is developed under the `y-crdt` umbrella ([The y-crdt organization](https://github.com/y-crdt)) and brings basic and complex shared types (Array, Map, Text, XML) to Ruby. The encoded updates and state vectors are a 100% compatible with [Y.js](https://github.com/yjs/yjs) and therefore state can be synced between the JavaScript frontend and the Ruby backend in a seamless way. This allows us to e.g. add, update, or remove labels via the GitLab UI but also with background jobs (some bot adding a label concurrently).

- [Project](https://gitlab.com/gitlab-org/incubation-engineering/real-time-editing/yrb)

### The `y-rb_redis` gem

**Current State:** Started

This [Ruby](https://www.ruby-lang.org/en/) gem is developed under the `y-crdt` umbrella ([The y-crdt organization](https://github.com/y-crdt)). It is a thin abstraction layer to allow persisting changesets to Redis. The updates produced by `y-crdt` documents are stored as easy to store and fetch elements in a list. This allows us to persist and restore documents efficiently.

- [Project](https://gitlab.com/gitlab-org/incubation-engineering/real-time-editing/yrb-redis)

### Awareness

**Current State:** Review process

Starting a collaborative editing session should be exactly the same as opening and editing an issue today. In case a second user starts to work on an issue, the backend will create a *collaborative editing session* users automatically and makes sure that all changes (deltas) are synced between all participating parties.

Awareness helps us to see who we are collaborating with and how. It helps us answering the following questions:
- Who is online and collaborating with me?
- Who is actively participating?

#### TODO

* UX: Create a session and awareness model that allows collaborators to "see" each other and their current state (present/away).

### Editor landscape

**Current State:** Started

To determine and replicate a change to the description field, we need to encode everything that a user does as a change event (delta). Y.js uses a binary encoded representation to optimize payloads, but essentially it is a list of operations. One example of how this looks like is the [Quill Delta Format](https://quilljs.com/docs/delta/).

The GitLab UI does not rely on a uniform text editor but instead offeres a mix of `textarea`, `tiptap/prosemirror`, `Web IDE`, …, multiple editor modes (Raw, WYSISWYG), and post-edit transforms (Markdown parser → references). We need to build a transform layer to broadcast changes made to a text field in a conflict-free format, and reversly, apply incoming changes seamlessly in the editor.

#### TODO

* Use the concept of [progressive enhancement](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement). Real-time is a feature of the editor, not the product.
* Move parser to frontend (WASM)
* Write a binding (transform) that allows to incrementally receive and apply updates in any text editing mode

## Links

- [Epic](https://gitlab.com/groups/gitlab-org/incubation-engineering/real-time-editing/-/epics?state=opened&page=1&sort=start_date_desc)
- [Issues](https://gitlab.com/gitlab-org/incubation-engineering/real-time-editing/real-time-editing-issue-descriptions/-/issues)

### Reading List

* [Hybrid Anxiety and Hybrid Optimism: The Near Future of Work](https://future.a16z.com/hybrid-anxiety-optimism-future-of-work/)
